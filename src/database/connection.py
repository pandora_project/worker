import os
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session

SQLALCHEMY_DATABASE_URL = "postgresql+psycopg2://" + os.environ[
    "DB_USER"] + ":" + os.environ["DB_PASSWORD"] + "@" + os.environ[
        "DB_HOST"] + "/" + os.environ["DB_NAME"]

engine = create_engine(SQLALCHEMY_DATABASE_URL,)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


class SessionFactory:

    def __init__(self):
        self.session = SessionLocal()

    def __enter__(self) -> Session:
        return self.session

    def __exit__(self, exc_type, exc_value, traceback):
        self.session.close()
