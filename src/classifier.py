import os

from PIL import Image
from torchvision import transforms
import torch


class Classifier():

    def __init__(self):
        self.model = torch.hub.load('pytorch/vision:v0.4.2',
                                    'googlenet',
                                    pretrained=True)
        self.model.eval()

    def predict(self, filename: str):
        input_image = Image.open(filename)
        preprocess = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225]),
        ])
        input_tensor = preprocess(input_image)
        input_batch = input_tensor.unsqueeze(0)

        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        input_batch = input_batch.to(device)
        self.model.to(device)

        with torch.no_grad():
            output = self.model(input_batch)
            class_index = int(
                torch.nn.functional.softmax(output[0], dim=0).argmax())

        pathway = os.path.dirname(os.path.abspath(__file__))
        pathway = os.path.join(pathway, 'synset_words.txt')
        with open(pathway, 'r') as f:
            class_name = f.readlines()[class_index].split(maxsplit=1)[1]
        return class_name