import urllib
import os
import json

import pika

from src.classifier import Classifier
from src.database import models
from src.database.connection import SessionFactory

queue_name = 'task_queue'

if __name__ == "__main__":

    def callback(ch, method, properties, body):
        body = json.loads(body)
        filename = 'temp.jpg'
        urllib.request.urlretrieve(body['url'], filename)

        response = classifier.predict(filename)
        print(response)

        with SessionFactory() as session:
            task = session.query(models.Task).filter_by(id=body['id']).first()
            task.response = response
            session.commit()

    classifier = Classifier()

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=os.environ['MQ_IP']))
    channel = connection.channel()

    channel.queue_declare(queue=queue_name)

    channel.basic_consume(queue=queue_name,
                          on_message_callback=callback,
                          auto_ack=True)

    print(' [*] Waiting for tasks. To exit press CTRL+C')
    channel.start_consuming()
